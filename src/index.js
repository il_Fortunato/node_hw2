const dotenv = require('dotenv').config({ path: './.env' });
const fs = require('fs');
const path = require('path');
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const { authRouter } = require('./routers/authRouter');
const { notesRouter } = require('./routers/notesRouter');
const { usersRouter } = require('./routers/usersRouter');

const PORT = process.env.PORT;
const CONNECT_LINK = process.env.DB_CONNECT_LINK;

mongoose.connect(CONNECT_LINK);

const logger = fs.createWriteStream(path.join(__dirname, 'history.log'), { flags: 'a' });

const errorHandler = (err, req, res, next) => {
  console.error(err);
  res.status(err.statusCode)
    .send({
      message: err.message,
    });
};

const start = () => {
  try {
    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

app.use(express.json());
app.use(morgan('combined', { stream: logger }));
app.use('/api/notes', notesRouter);
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);
app.use(errorHandler);

start();
