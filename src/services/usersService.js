const bcrypt = require('bcryptjs');
const { User } = require('../models/User');
const { Credentials } = require('../models/Credentials');

const getUser = async (req, res, next) => {
  const user = await User.findOne({ _id: req.user.userId });

  if (!user) {
    next({
      message: 'Bad request',
      statusCode: 400,
    });
  }

  res.status(200)
    .send({
      user: {
        _id: user.id,
        username: user.username,
        createdDate: user.createdDate,
      },
    });
};

const deleteUser = async (req, res, next) => {
  await Credentials.findByIdAndDelete(req.user.userId);

  await User.findByIdAndDelete(req.user.userId)
    .then(() =>
      res.status(200)
        .send({
          message: 'Success',
          statusCode: 200,
        }));
};

const changePassword = async (req, res, next) => {
  const {
    oldPassword,
    newPassword,
  } = req.body;

  const user = await Credentials.findOne({ _id: req.user.userId });

  if (!user) {
    next({
      message: 'Bad request',
      statusCode: 400,
    });
  }

  const matchPasswords = await bcrypt.compare(oldPassword, user.password);

  if (!matchPasswords) {
    next({
      message: 'Old password is incorrect',
      statusCode: 400,
    });
    return;
  }

  await Credentials.findByIdAndUpdate(req.user.userId, {
    password: await bcrypt.hash(newPassword, 10),
  })
    .then(() =>
      res.status(200)
        .send({
          message: 'Success',
          statusCode: 200,
        }));
};

module.exports = {
  getUser,
  deleteUser,
  changePassword,
};
