const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { Credentials } = require('../models/Credentials');
const { User } = require('../models/User');

const registerUser = async (req, res, next) => {
  const {
    username,
    password
  } = req.body;

  const credentials = new Credentials({
    username,
    password: await bcrypt.hash(password, 10),
  });

  const user = new User({
    _id: credentials.id,
    username,
    createdDate: Date.now(),
  });

  const currentUser = await Credentials.findOne({ username: req.body.username });

  if (currentUser) {
    next({
      message: 'Such user already exists',
      statusCode: 403,
    });
    return;
  }

  user.save();
  credentials.save()
    .then(() =>
      res.status(200)
        .send({
          message: 'Success',
        }));
};

const loginUser = async (req, res, next) => {
  const user = await Credentials.findOne({ username: req.body.username });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = {
      username: user.username,
      userId: user.id
    };

    const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);

    res.status(200)
      .send({
        message: 'Success',
        jwt_token: jwtToken,
      });
  } else {
    next({
      message: 'Not authorized',
      statusCode: 400,
    });
  }
};

module.exports = {
  registerUser,
  loginUser,
};
