const { Notes } = require('../models/Notes');

const getNotes = async (req, res, next) => {
  const {
    offset,
    limit,
  } = req.body;

  const notes = await Notes.find({ userId: req.user.userId })
    .skip(offset)
    .limit(limit);

  if (!notes) {
    next({
      message: 'Bad request',
      statusCode: 400,
    });
  }

  res.status(200)
    .send({
      offset,
      limit,
      count: notes.length,
      notes,
    });
};

const addNote = async (req, res, next) => {
  const { text } = req.body;

  const note = new Notes({
    userId: req.user.userId,
    text,
    createdDate: Date.now(),
  });

  await note.save()
    .then(() =>
      res.status(200)
        .send({
          message: 'Success',
          statusCode: 200,
        }));
};

const getNote = async (req, res, next) => {
  const { id } = req.params;
  const note = await Notes.findOne({
    _id: id,
    userId: req.user.userId,
  });

  res.status(200)
    .send({ note });
};

const updateNote = async (req, res, next) => {
  await Notes.findByIdAndUpdate(req.params.id, {
    text: req.body.text,
  })
    .then(() =>
      res.status(200)
        .send({
          message: 'Success',
          statusCode: 200,
        }));
};

const checkNote = async (req, res, next) => {
  await Notes.findByIdAndUpdate(req.params.id, {
    completed: !this.completed,
  })
    .then(() =>
      res.status(200)
        .send({
          message: 'Success',
          statusCode: 200,
        }));
};

const deleteNote = async (req, res, next) => {
  await Notes.findByIdAndDelete(req.params.id)
    .then(() =>
      res.status(200)
        .send({
          message: 'Success',
          statusCode: 200,
        }));
};

module.exports = {
  getNotes,
  addNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
};
