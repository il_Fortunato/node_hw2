const mongoose = require('mongoose');

const User = mongoose.model('Users', {
  username: {
    type: String,
    required: true,
    unique: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {
  User,
};
