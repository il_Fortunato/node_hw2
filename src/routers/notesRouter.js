const express = require('express');
const router = express.Router();
const {
  getNotes,
  addNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
} = require('../services/notesService');
const { authMiddleware } = require('../middleware/authMiddle');

router.get('/', authMiddleware, getNotes);

router.post('/', authMiddleware, addNote);

router.get('/:id', authMiddleware, getNote);

router.put('/:id', authMiddleware, updateNote);

router.patch('/:id', authMiddleware, checkNote);

router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
