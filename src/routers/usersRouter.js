const express = require('express');
const router = express.Router();
const { getUser, deleteUser, changePassword } = require('../services/usersService');
const { authMiddleware } = require('../middleware/authMiddle');

router.get('/me', authMiddleware, getUser);

router.delete('/me', authMiddleware, deleteUser);

router.patch('/me', authMiddleware, changePassword);

module.exports = {
  usersRouter: router,
};
