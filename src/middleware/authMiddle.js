const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  const { authorization } = req.headers;
  const [, token] = authorization.split(' ');

  if (!authorization) {
    res.status(401)
      .send({
        message: 'Please, provide authorization header',
      });
  }

  if (!token) {
    res.status(401)
      .send({
        message: 'Please, include token to request',
      });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.SECRET_KEY);
    req.user = {
      userId: tokenPayload.userId,
      username: tokenPayload.username,
    };
    next();
  } catch (err) {
    res.status(401)
      .send({
        message: err.message,
      });
  }
};

module.exports = {
  authMiddleware,
};
